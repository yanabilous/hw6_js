function createNewUser() {
    let firstName = prompt("enter your name!");
    let lastName = prompt("enter your last surname!");
    let birthday = prompt("enter your  birthday - dd.mm.yyyy!");


    let newUser = {
        firstName,
        lastName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        birthday,
        getAge() {

            const [birth_day, birth_month, birth_year] = this.birthday.split(".");
            let bDayDate = new Date(birth_year, birth_month, birth_day);
            let today = new Date();
            let diggInMs = today - bDayDate;
            let ageDate = new Date(diggInMs);
            return Math.abs(ageDate.getUTCFullYear() - 1970);

        },
        

        getPassword() {
            let arrDate = this.birthday.split(".");
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + arrDate[2];
        }
    };

    return newUser;
}

let user1 = createNewUser();
// let loginOfUser1 = user1.getLogin();
let getAgeOfUser1 = user1.getAge();
let getPasswordOfUser1 = user1.getPassword();


console.log(getAgeOfUser1);
console.log(getPasswordOfUser1);

